<?php
	include("php/db.php");
	include("php/oidc.php");
	include("php/user.php");
	include("php/etc.php");

	$userinfo = check_and_get_userinfo($db);
	if (!$userinfo) {
		$state = md5(rand());
		$nonce = md5(rand());
		$session = serialize(array(
			"state" => $state,
			"nonce" => $nonce
		));
		setcookie("session", $session);
	}

?>

<!doctype html>
<html lang="en">
<head>
	<title>Connectory</title>
	<link href="style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<h1>Connectory</h1>
	<?php
		if ($userinfo) {
	?>
	<p>Hi, <?php echo preferred_name($userinfo); ?>. <a href="php/logout.php">Log out</a></p>
	<p><a href="profile.php?kerberos=<?php echo $userinfo['kerberos']; ?>">Your Profile</a></p>
	<form action="search.php" method="get">
		<p><input type="text" name="query" placeholder="Name or Kerberos">
		<input type="submit" value="Search"></p>
	</form>
	<?php
		$stmt = $db->prepare("SELECT COUNT(*) FROM connections WHERE requestee_sub = ? AND status = 2");
		$stmt->execute(array(
			$userinfo["sub"]
		));
		$request_result = $stmt->fetch(PDO::FETCH_NUM);
		$request_count = (int) $request_result[0];
		if ($request_count > 0) {
	?>
	<p><a href="connection_requests.php">View Connection Requests (<?php echo $request_count; ?>)</a></p>
	<?php } 
		$connected_user_subs = get_connected_user_subs($db, $userinfo["sub"]);
	?>
	<form action="php/index_action.php" method="post">
		<p><input type="submit" name="action" value="Download Selected Connections as vCard">
		<input type="submit" name="action" value="Download Your Directory as vCard">
		<input type="submit" name="action" value="Remove Selected Connections"></p>
		<input type="hidden" name="referrer" value="<?php echo current_url($_SERVER); ?>">
		<div class="tablediv">
		<table>
			<tr>
				<td>
					<a href="index.php?select=all">Select All</a><br>
					<a href="index.php?select=none">Select None</a>
				</td>
				<th>Name</th>
				<th>MIT Email</th>
				<th>Other Email</th>
				<th>Phone Number</th>
				<th></th>
			</tr>
			<?php
			$connected_userinfos = array();
			foreach ($connected_user_subs as $connected_user_sub) {
				$connected_userinfos[] = get_userinfo($db, $connected_user_sub);
			}

			$connected_userinfos = sortBy($connected_userinfos, 'name');

			foreach ($connected_userinfos as $connected_userinfo) {
				echo '<tr><td><input type="checkbox" name="sub[]" value="' . $connected_userinfo["sub"] . '"';
				if (isset($_GET["select"]))
				{
					if ($_GET["select"] == "all")
					{
						echo " checked ";
					} elseif ($_GET["select"] == "none")
					{
						echo ' autocomplete="off" ';
					}
				}
				echo '></td>';
				echo '<td><a href="profile.php?kerberos=' . $connected_userinfo["kerberos"] . '">' . full_preferred_name($connected_userinfo) . "</a></td>";
				echo '<td><a href="mailto:' . $connected_userinfo["email"] . '">' . $connected_userinfo["email"] . "</a></td>";
				echo '<td><a href="mailto:' . $connected_userinfo["other_email"] . '">' . $connected_userinfo["other_email"] . "</a></td>";
				echo "<td>" . $connected_userinfo["phone"] . "</td>";
				echo "\n";
			} ?>
		</table>
		</div>
	</form>
	<p></p>
	<?php } else { 

	?>
	<form action="php/pre_login.php" method="post">
		<p><input type="checkbox" name="persistent" value="yes">
		<label for="persistent">Stay logged in</label></p>
		<input type="submit" value="Log In">
	</form>
	<?php 
		} 
		include("php/footer.php"); 
	?>
	
</body>
</html>
