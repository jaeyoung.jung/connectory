<?php 
	include("php/db.php");
	include("php/user.php");
	include("php/etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!isset($_GET["sub"]) && !isset($_GET["kerberos"]))
	{
		header('Location: ' . INDEX_URL);
	}


	$self_profile = false;

	if ($userinfo)
	{
		if (isset($_GET['sub']))
		{
			if ($_GET['sub'] == $userinfo['sub'])
			{
				$self_profile = true;
				$requested_userinfo = $userinfo;
			}
		} elseif (isset($_GET['kerberos']))
		{
			if ($_GET['kerberos'] == $userinfo['kerberos'])
			{
				$self_profile = true;
				$requested_userinfo = $userinfo;
			}
		}
	}
	if (!$self_profile)
	{
		if (isset($_GET["sub"]))
		{
			$requested_userinfo = get_userinfo($db, $_GET["sub"]);
		} elseif (isset($_GET["kerberos"]))
		{
			$requested_userinfo = get_userinfo_by_kerberos($db, $_GET["kerberos"]);
		}
		if ($userinfo)
		{
			$connected_status = connected_status($db, $userinfo["sub"], $requested_userinfo["sub"]);
		}
	}

	
?>
<!doctype html>
<html lang="en">
<head>
	<title>Connectory: <?php if ($requested_userinfo) { echo full_preferred_name($requested_userinfo); } else { echo "Not Found"; } ?></title>
	<link href="style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
	if ($requested_userinfo)
	{ ?>

	<h1><?php echo full_preferred_name($requested_userinfo); ?></h1>
	<?php
		if (!$userinfo)
		{ ?>
		<p>Please log in to view <?php echo preferred_name($requested_userinfo); ?>'s profile.</p>
		<?php } elseif ($self_profile)
		{ ?>
		<form action="php/edit_profile_action.php" method="post">
			<input type="hidden" name="referrer" value="<?php echo current_url($_SERVER); ?>">
			<table>
				<tr><td>MIT Email:</td><td><input type="email" name="email" value="<?php echo $userinfo["email"]; ?>" readonly></td></tr>
				<tr><td>Other Email:</td><td><input type="email" name="other_email" maxlength="60" value="<?php echo $userinfo["other_email"]; ?>"></td></tr>
				<tr><td>Phone Number:</td><td><input type="tel" name="phone" maxlength="20" value="<?php echo $userinfo["phone"]; ?>"></td></tr>
				<tr><td>Nickname:</td><td><input type="text" name="nickname" maxlength="40" value="<?php echo $userinfo["nickname"]; ?>"></td></tr>
				<tr><td>Address:</td><td><textarea name="address" maxlength="100"><?php echo $userinfo["address"]; ?></textarea></td></tr>
				<tr><td>Website:</td><td><input type="text" name="website" maxlength="40" value="<?php echo $userinfo["website"]; ?>"></td></tr>
				<tr><td>Blurb:</td><td><textarea name="blurb" maxlength="100" rows="6" cols="40"><?php echo $userinfo["blurb"]; ?></textarea></td></tr>
				<tr><td>Profile URL:</td><td><input type="text" readonly size="40" value="<?php echo INDEX_URL . 'profile.php?kerberos=' . $userinfo['kerberos']; ?>"></td></tr>

			</table>
			<p><input type="submit" value="Update"></p>
		</form>
		<?php } elseif ($connected_status == 2)
		{ ?>
		<p>You have requested a connection with <?php echo preferred_name($requested_userinfo); ?>. You will be able to see their profile when they accept your request. <a href="php/delete_connection.php?<?php echo safe_build_query(array('referrer' => urlencode(current_url($_SERVER)), 'sub' => $requested_userinfo["sub"])); ?>">Cancel request</a></p>
		<?php } elseif ($connected_status == 3)
		{ ?>
		<p><?php echo preferred_name($requested_userinfo);?> has requested to connect with you. <a href="php/accept.php?<?php echo safe_build_query(array('referrer' => urlencode(current_url($_SERVER)), 'sub' => $requested_userinfo["sub"])); ?>">Accept</a> their request to view their profile, or <a href="php/delete_connection.php?<?php echo safe_build_query(array('referrer' => urlencode(current_url($_SERVER)), 'sub' => $requested_userinfo["sub"])); ?>">reject</a> their request.</p>
		<?php } elseif ($connected_status == 1)
		{ ?>
		<table>
			<tr><td><b>MIT Email:</b></td><td><a href="mailto:<?php echo $requested_userinfo["email"]; ?>"><?php echo $requested_userinfo["email"]; ?></a></td></tr>
		<?php if ($requested_userinfo["other_email"] != '')
		{ ?>	
			<tr><td><b>Other Email:</b></td><td><a href="mailto:<?php echo $requested_userinfo["other_email"]; ?>"><?php echo $requested_userinfo["other_email"]; ?></a></td></tr>
		<?php }
		 if ($requested_userinfo["phone"] != '')
		 { ?>
			<tr><td><b>Phone Number:</b></td><td><?php echo $requested_userinfo["phone"]; ?></td></tr>
		<?php }
		 if ($requested_userinfo["nickname"] != '')
		 { ?>
			<tr><td><b>Nickname:</b></td><td><?php echo $requested_userinfo["nickname"]; ?></td></tr>
		<?php }
		 if ($requested_userinfo["address"] != '')
		 { ?>
			<tr><td><b>Address:</b></td><td><pre><?php echo $requested_userinfo["address"]; ?></pre></td></tr>
		<?php }
		 if ($requested_userinfo["website"] != '')
		 { ?>
			<tr><td><b>Website:</b></td><td><a href="<?php echo full_url($requested_userinfo["website"]); ?>"><?php echo $requested_userinfo["website"]; ?></a></td></tr>
		<?php }
		 if ($requested_userinfo["blurb"] != '')
		 { ?>
			<tr><td><b>Blurb:</b></td><td><pre><?php echo $requested_userinfo["blurb"]; ?></pre></td></tr>
		<?php } ?>

		</table>
		<form action="php/vcard_one.php" method="get">
			<input type="hidden" name="sub" value="<?php echo $requested_userinfo["sub"]; ?>">
			<p><input type="submit" value="Download vCard"></p>
		</form>
		<form action="php/delete_connection.php" method="get">
			<input type="hidden" name="referrer" value="<?php echo current_url($_SERVER); ?>">
			<input type="hidden" name="sub" value="<?php echo $requested_userinfo["sub"]; ?>">
			<p><input type="submit" value="Remove Connection"></p>
		</form>
		<?php } elseif ($connected_status == 0)
		{ ?>
		<p>You are not connected to <?php echo preferred_name($requested_userinfo); ?>. <a href="php/request.php?<?php echo safe_build_query(array('referrer' => urlencode(current_url($_SERVER)), 'sub' => $requested_userinfo["sub"])); ?>">Request</a> a connection to see their profile.</p>
	<?php } 
		} else
		{ ?>
	<p><b>The requested user was not found.</b></p>
	<?php } ?>
	<p><a href="index.php">Home</a></p>
	<?php include("php/footer.php"); ?>
</body>
</html>
