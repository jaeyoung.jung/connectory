<!doctype html>
<html lang="en">
<head>
	<title>Connectory Terms of Service</title>
	<link href="style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<h1>Connectory Terms of Service</h1>
	<textarea rows="10" cols="100" readonly>
Trust this service as much as you trust me, Jaeyoung Jung.

Assume that any data you enter into this service is accessible by me, because it is.
		
The only data that are stored in the database are data that you enter into this service.
		
Those data are stored so that they can be displayed to your connections.

Do not enter any data that you would not want me to have access to.

I will do my best to keep your data from being accessed by people who should not have access to them, but I cannot make any promises.
		
If you want your data to be erased from the database completely, email me at jungj (at) mit (dot) edu.

By using this service, you understand and accept these terms of service.
	</textarea>
	<p><a href="index.php">Home</a></p>
	<?php include("php/footer.php"); ?>
</body>
</html>
		
