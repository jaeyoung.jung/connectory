<?php
	include('config.php');

	function current_url($server) {
		return (isset($server['HTTPS']) && $server['HTTPS'] === 'on' ? "https" : "http") . "://" . $server["HTTP_HOST"] . $server["REQUEST_URI"];
	}



	function build_sorter($key) {
		return function ($array1, $array2) use ($key) {
			return strnatcmp($array1[$key], $array2[$key]);
		};
	}

	function sortBy($array, $key) {
		usort($array, build_sorter($key));
		return $array;
	}

	function full_url($url) 
	{
		$exploded_url = explode('://', $url);
		if (sizeof($exploded_url) == 1) {
			return 'http://' . $url;
		}
		return $url;
	}

	function safe_build_query($array)
	{
		return htmlspecialchars(http_build_query($array));
	}

?>
