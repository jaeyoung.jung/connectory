<?php 
	include("db.php");
	include("user.php");
	include("etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo || empty($_POST))
	{
		header("Location: " . INDEX_URL);
	}


	$stmt = $db->prepare("UPDATE users SET other_email = ?, phone = ?, nickname = ?, address = ?, website = ?, blurb = ? WHERE sub = ?");
	$stmt->execute(array(
		strip_tags($_POST["other_email"]),
		strip_tags($_POST["phone"]),
		strip_tags($_POST["nickname"]),
		$_POST["address"],
		addslashes($_POST["website"]),
		strip_tags($_POST["blurb"]),
		$userinfo["sub"]
	));
	header('Location: ' . $_POST['referrer']);
?>
