<?php
	include("db.php");
	include("user.php");
	include("etc.php");
	include("vcard.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo || !isset($_GET["sub"])) {
		header("Location: " . INDEX_URL);
	}

	$status = connected_status($db, $userinfo["sub"], $_GET["sub"]);
	if ($status == 1) {
		header("Content-type: text/x-vcard");
		$requested_userinfo = get_userinfo($db, $_GET["sub"]);
		header("Content-disposition: attachment; filename=connectory_" . preg_replace("/\s+/", "_", preferred_name($requested_userinfo)) . ".vcf");

		echo build_vcard($requested_userinfo);

	} else {
		die("Access denied");
	}

?>