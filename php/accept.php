<?php
	include("db.php");
	include("user.php");
	include("etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo) {
		header("Location: " . INDEX_URL);
	}

	if (isset($_GET["sub"])) {
		$stmt = $db->prepare("UPDATE connections SET status = 1 WHERE requester_sub = ? AND requestee_sub = ?");
		$stmt->execute(array(
			$_GET["sub"],
			$userinfo["sub"]
		));
	}
	
	header("Location: " . urldecode($_GET["referrer"]));
?>