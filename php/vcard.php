<?php
	require_once __DIR__ . "/../vendor/autoload.php";

	use JeroenDesloovere\VCard\VCard;


    function build_vcard($requested_userinfo)  {
        $vcard = new VCard();
    	if (isset($requested_userinfo["nickname"]) && $requested_userinfo["nickname"] != "") {
            $vcard->addName($requested_userinfo["family_name"], $requested_userinfo["given_name"], $requested_userinfo["middle_name"], "", "(" . $requested_userinfo["nickname"] . ")");
        } else {
            $vcard->addName($requested_userinfo["family_name"], $requested_userinfo["given_name"], $requested_userinfo["middle_name"]);
        }
		$vcard->addEmail($requested_userinfo["email"], "WORK");
		if (isset($requested_userinfo["other_email"]) && $requested_userinfo["other_email"] != "") {
			$vcard->addEmail($requested_userinfo["other_email"], "HOME");
		}
		if (isset($requested_userinfo["phone"]) && $requested_userinfo["phone"] != "") {
			$vcard->addPhoneNumber($requested_userinfo["phone"]);
		}
		if (isset($requested_userinfo["address"]) && $requested_userinfo["address"] != "") {
			$vcard->addAddress($street = $requested_userinfo["address"], "", "", "", "", "", "", "HOME");
		}
		if (isset($requested_userinfo["website"]) && $requested_userinfo["website"] != "") {
			$vcard->addURL($requested_userinfo["website"]);
		}
        return $vcard->getOutput();
    }

?>
