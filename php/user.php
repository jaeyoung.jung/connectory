<?php
	//user functions library

	function get_sub($db, $login) {
		$login_array = unserialize($login);

		$stmt = $db->prepare("SELECT * FROM logins WHERE id = ?");
		$stmt->execute(array($login_array["id"]));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if (password_verify($login_array["pass"], $row["hash"])) {
			return $row["sub"];
		}
		return false;
	}
	
	function get_userinfo($db, $sub) {
		$stmt = $db->prepare("SELECT * FROM users WHERE sub = ?");
		$stmt->execute(array($sub));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	function get_userinfo_by_kerberos($db, $sub) {
		$stmt = $db->prepare("SELECT * FROM users WHERE kerberos = ?");
		$stmt->execute(array($sub));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		return $row;
	}

	function check_and_get_userinfo($db) {
		global $_COOKIE;

		if (isset($_COOKIE["login"])) {
			$sub = get_sub($db, $_COOKIE["login"]);
			if ($sub) {
				$userinfo = get_userinfo($db, $sub);
				return $userinfo;
			} else {
				unset($_COOKIE["login"]);
				setcookie("login", "", time() - 3600);
				return false;
			}
		}
		return false;
	}

	function preferred_name($userinfo) {
		if (isset($userinfo["nickname"]) && $userinfo["nickname"] != "") {
			return $userinfo["nickname"];
		}
		return $userinfo["given_name"];
	}

	function full_preferred_name($userinfo) {
		$name = $userinfo["name"];

		if (isset($userinfo["nickname"]) && $userinfo["nickname"] != "") {
			$name .= " (" . $userinfo["nickname"] . ")";
		}
		return $name;
	}

	function connected_status($db, $sub1, $sub2) {
		// 1: connected
		// 2: request sent
		// 3: request received
		// 0: no connection
		$stmt = $db->prepare("SELECT * FROM connections WHERE requester_sub = ? AND requestee_sub = ?");
		$stmt->execute(array(
			$sub1,
			$sub2
		));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if (sizeof($result) > 0) {
			if ($result[0]["status"] == "1") {
				return 1;
			} elseif ($result[0]["status"] == "2") {
				return 2;
			}
		}
		$stmt = $db->prepare("SELECT * FROM connections WHERE requestee_sub = ? AND requester_sub = ?");
		$stmt->execute(array(
			$sub1,
			$sub2
		));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (sizeof($result) > 0) {
			if ($result[0]["status"] == "1") {
				return 1;
			} elseif ($result[0]["status"] == "2") {
				return 3;
			}
		}
		return 0;
	}
	function get_connected_user_subs($db, $sub) {
		$connected_user_subs = array();
		$stmt = $db->prepare("SELECT * FROM connections WHERE status = 1 AND requester_sub = ?");
		$stmt->execute(array(
			$sub
		));
		$connections = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($connections as $connection) {
			$connected_user_subs[] = $connection["requestee_sub"];
		}
		$stmt = $db->prepare("SELECT * FROM connections WHERE status = 1 AND requestee_sub = ?");
		$stmt->execute(array(
			$sub
		));
		$connections = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach ($connections as $connection) {
			$connected_user_subs[] = $connection["requester_sub"];
		}
		return $connected_user_subs;
	}

	function delete_connection($db, $sub1, $sub2) {
		$stmt = $db->prepare("DELETE FROM connections WHERE (requestee_sub = ? AND requester_sub = ?) OR (requestee_sub = ? AND requester_sub = ?)");
		$stmt->execute(array(
			$sub1,
			$sub2,
			$sub2,
			$sub1
		));
	}

?>
