<?php
	include("db.php");
	include("user.php");
	include("etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo) {
		header("Location: " . INDEX_URL);
	}
	
	if (isset($_GET["sub"])) {
		$stmt = $db->prepare("INSERT INTO connections (requester_sub, requestee_sub, status) VALUES (?, ?, 2)");
		$stmt->execute(array(
			$userinfo["sub"],
			$_GET["sub"]
		));
	}

	header("Location: " . urldecode($_GET["referrer"]));
?>
