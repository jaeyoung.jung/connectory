<?php
	include("db.php");
	include("user.php");
	include("etc.php");
	include("vcard.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo || empty($_POST))
	{
		header("Location: " . INDEX_URL);
	}

	if ($_POST["action"] == "Remove Selected Connections")
	{
		if (!isset($_POST["sub"]))
		{
			header("Location: " . INDEX_URL);
		}

		foreach ($_POST["sub"] as $sub)
		{
			delete_connection($db, $userinfo["sub"], $sub);
		}
		header("Location: " . $_POST["referrer"]);
	} elseif ($_POST["action"] == "Download Selected Connections as vCard")
	{
		if (!isset($_POST["sub"]))
		{
			header("Location: " . INDEX_URL);
		}

		$output = "";
		foreach ($_POST["sub"] as $requested_sub) 
		{
			if (connected_status($db, $userinfo["sub"], $requested_sub) == 1)
			{
				$requested_userinfo = get_userinfo($db, $requested_sub);
				$output  .= build_vcard($requested_userinfo) . "\n";
			} else
			{
				die("Access denied");
			}
		}
		header("Content-type: text/x-vcard");
		header("Content-disposition: attachment; filename=connectory_" . date("Y-m-d") . ".vcf");
		echo $output;
	} else 
	{
		$requested_user_subs = get_connected_user_subs($db, $userinfo["sub"]);

		$output = "";
		foreach ($requested_user_subs as $requested_sub) 
		{
			$requested_userinfo = get_userinfo($db, $requested_sub);
			$output  .= build_vcard($requested_userinfo) . "\n";
		}
		header("Content-type: text/x-vcard");
		header("Content-disposition: attachment; filename=connectory_" . date("Y-m-d") . ".vcf");
		echo $output;
	}
?>
