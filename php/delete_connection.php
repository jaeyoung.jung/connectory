<?php
	include("db.php");
	include("user.php");
	include("etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo) {
		header("Location: " . INDEX_URL);
	}


	if (isset($_GET["sub"])) {
		delete_connection($db, $userinfo["sub"], $_GET["sub"]);
	}

	header("Location: " . urldecode($_GET["referrer"]));
?>