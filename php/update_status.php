<?php
	include("db.php");
	include("user.php");
	include("etc.php");

	$userinfo = check_and_get_userinfo($db);

	if (!$userinfo || empty($_POST))
	{
		header("Location: " . INDEX_URL);
	}

	$stmt = $db->prepare("UPDATE users SET status = ? WHERE sub = ?");
	$stmt->execute(array(
		$_POST["status"],
		$userinfo["sub"]
	));

	header('Location: '. $_POST['referrer']);
?>