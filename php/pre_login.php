<?php
	include("oidc.php");
	include("etc.php");

	$session_cookie = unserialize($_COOKIE["session"]);
	$state = $session_cookie["state"];
	$nonce = $session_cookie["nonce"];

	if (isset($_POST["persistent"]) && $_POST["persistent"] == "yes") {
		$state .= ".persistent";
	}

	$href = "https://oidc.mit.edu/authorize?client_id=" . CLIENT_ID . "&response_type=code&scope=openid%20profile%20email&redirect_uri=" . urlencode(LOGIN_PAGE_URL) . "&state=" . $state . "&nonce=" . $nonce;

	header("Location: " . $href);
?>