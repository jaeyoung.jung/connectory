<?php
	include("php/db.php");
	include("php/user.php");
	include("php/etc.php");

	$userinfo  = check_and_get_userinfo($db);

?>
<!doctype html>
<html lang="en">
<head>
	<title>Connectory: Connection Requests</title>
	<link href="style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<h1>Connection Requests</h1>
	<?php
		$connected_user_subs = array();
		$stmt = $db->prepare("SELECT * FROM connections WHERE status = 2 AND requestee_sub = ?");
		$stmt->execute(array(
			$userinfo["sub"]
		));
		$connections = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (sizeof($connections) > 0) {

			foreach ($connections as $connection) {
				$connected_user_subs[] = $connection["requester_sub"];
			} 
	?>

	<table>
		<tr>
			<th>Name</th>
			<th></th>
			<th></th>
		</tr>
		<?php
			foreach ($connected_user_subs as $connected_user_sub) {
				$connected_userinfo = get_userinfo($db, $connected_user_sub);
				echo '<tr><td><a href="profile.php?kerberos=' . $connected_userinfo['kerberos'] . '">' . full_preferred_name($connected_userinfo) . "</a></td>";
				echo '<td><a href="php/accept.php?' . safe_build_query(array(
					"referrer" => urlencode(current_url($_SERVER)),
					"sub" => $connected_user_sub
				)) . '">Accept Request</a></td>';
				echo '<td><a href="php/delete_connection.php?' . safe_build_query(array(
					"referrer" => urlencode(current_url($_SERVER)),
					"sub" => $connected_user_sub
				)) . '">Reject Request</a></td></tr>';
		} ?>
	</table>
	<?php } else { ?>

		<p><i>You have no connection requests at this time.</i></p>

	<?php } ?>
	<p><a href="index.php">Home</a></p>
	<?php include("php/footer.php"); ?>
</body>
</html>
