<?php
	include("php/db.php");
	include("php/user.php");
	include("php/etc.php");
	$userinfo = check_and_get_userinfo($db);


?>
<!doctype html>
<html lang="en">
<head>
	<title>Connectory: Search Results</title>
	<link href="style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<?php
		if (!$userinfo) { ?>
	<p>Please return to Home and log in.</p>
	<?php 
		} else {
			if (isset($_GET["query"]) && strlen($_GET["query"]) > 1) {
				$stmt = $db->prepare("SELECT * FROM users WHERE name LIKE ? OR nickname LIKE ? OR kerberos like ?");
				$stmt->execute(array(
					"%" . $_GET["query"] . "%",
					"%" . $_GET["query"] . "%",
					"%" . $_GET["query"] . "%"
				));
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				echo "<h1>Search Results for '" . $_GET["query"] . "':</h1>";
				if (sizeof($results) != 0) { ?>
	<table>
		<tr>
			<th>Name</th>
			<th>MIT Email</th>
			<th></th>
		</tr>
		<?php foreach ($results as $value) { ?>
		<tr>
			<td><a href="profile.php?kerberos=<?php echo $value["kerberos"]; ?>"><?php echo full_preferred_name($value);?></a></td>
			<td><a href="mailto:<?php echo $value["kerberos"]; ?>@mit.edu"><?php echo $value["kerberos"]; ?>@mit.edu</a></td>
			<td>
			<?php
				if ($value["sub"] != $userinfo["sub"]) {
					switch(connected_status($db, $userinfo["sub"], $value["sub"])) {
						case 0:
							?> <a href="php/request.php?<?php echo safe_build_query(array(
								"referrer" => urlencode(current_url($_SERVER)),
								"sub" => $value["sub"]
							)); ?>">Request connection</a> <?php
							break;
						case 1:
							echo "Connected";
							break;
						case 3:
							?> <a href="php/accept.php?<?php echo safe_build_query(array(
							"referrer" => urlencode(current_url($_SERVER)),
							"sub" => $value["sub"]
						)); ?>">Accept connection request</a> <?php
							break;
						case 2:
							?> <a href="php/delete_connection.php?<?php echo safe_build_query(array(
							"referrer" => urlencode(current_url($_SERVER)),
							"sub" => $value["sub"]
						)); ?>">Cancel connection request</a> <?php
							break;
					}
				} ?>
			</td>
		</tr>
		<?php } ?>

	</table>
	<?php } else { ?>
	<p><i>No results</i></p>
	<?php }} else { ?>
	<p>Please enter a query longer than 2 letters.</p>
	<?php }
	} ?>
	<p><a href="index.php">Home</a></p>
	<?php include("php/footer.php"); ?>
</body>
</html>
